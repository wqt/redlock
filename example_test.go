package redlock_test

import (
	"context"
	"fmt"
	"time"

	"gitee.com/wqt/redlock"
	"gitee.com/wqt/redlock/driver/redigo"
	"github.com/gomodule/redigo/redis"
)

func ExampleMutex_Exec() {
	redlock.SetLogger(&redlock.StdLogger{})
	// 初始化redigo的连接
	p := &redis.Pool{
		DialContext: func(ctx context.Context) (redis.Conn, error) {
			return redis.DialContext(ctx, "tcp", "127.0.0.1:6379",
				redis.DialDatabase(2),
			)
		},
		MaxIdle:     2,
		IdleTimeout: 2 * time.Minute,
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}

	taskName := func(id string) string {
		return fmt.Sprintf("task:%s", id)
	}
	// 创建一个工作任务锁
	workers := redlock.New(redigo.NewClient(p))
	lock := workers.NewMutex(taskName("exec"),
		// 请求超时时间
		redlock.WithTimeout(30*time.Second),
		// 锁的有效期
		redlock.WithExpires(60*time.Second),
		// 重试次数, 因此最大尝试次数是2+1
		redlock.WithRetries(2),
	)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	if err := lock.Exec(ctx, func(c context.Context) {
		select {
		case <-ctx.Done():
			return
		default:
		}
		fmt.Println("ok")
	}); err != nil {
		fmt.Println(err.Error())
	}

	// Output: ok
}

func ExampleMutex_Exec_extendCanceled() {
	// 初始化redigo的连接
	p := &redis.Pool{
		DialContext: func(ctx context.Context) (redis.Conn, error) {
			return redis.DialContext(ctx, "tcp", "127.0.0.1:6379",
				redis.DialDatabase(2),
			)
		},
		MaxIdle:     2,
		IdleTimeout: 2 * time.Minute,
	}
	taskName := func(id string) string {
		return fmt.Sprintf("task:%s", id)
	}
	// 创建一个工作任务锁
	workers := redlock.New(redigo.NewClient(p))
	lock := workers.NewMutex(taskName("exec:extend:canceled"),
		// 锁的有效期
		redlock.WithExpires(500*time.Millisecond),
		// 重试次数, 因此最大尝试次数是2+1
		redlock.WithRetries(2),
	)
	ctx, cancel := context.WithTimeout(context.Background(), 200*time.Millisecond)
	defer cancel()
	exit := make(chan struct{})
	err := lock.Exec(ctx, func(c context.Context) {
		defer close(exit)
		for i := 0; i < 30; i++ {
			select {
			case <-ctx.Done():
				fmt.Println("cancel")
				return
			default:
				time.Sleep(10 * time.Millisecond)
			}
		}
		fmt.Println("ok")
	})
	if err != nil {
		fmt.Println("err =", err.Error())
	}
	select {
	case <-exit:
	case <-ctx.Done():
		time.Sleep(10 * time.Millisecond)
	}

	// Unordered output:
	// err = context deadline exceeded
	// cancel
}

func ExampleMutex_Exec_extendOk() {
	// 初始化redigo的连接
	redlock.SetLogger(&redlock.StdLogger{})
	p := &redis.Pool{
		DialContext: func(ctx context.Context) (redis.Conn, error) {
			return redis.DialContext(ctx, "tcp", "127.0.0.1:6379",
				redis.DialDatabase(2),
			)
		},
		MaxIdle:     2,
		IdleTimeout: 2 * time.Minute,
	}
	taskName := func(id string) string {
		return fmt.Sprintf("task:%s", id)
	}
	// 创建一个工作任务锁
	workers := redlock.New(redigo.NewClient(p))
	lock := workers.NewMutex(taskName("exec:extend:ok"),
		// 请求超时时间
		redlock.WithTimeout(2*time.Second),
		// 锁的有效期
		redlock.WithExpires(500*time.Millisecond),
		// 重试次数, 因此最大尝试次数是2+1
		redlock.WithRetries(2),
	)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	exit := make(chan struct{})
	err := lock.Exec(ctx, func(ctx context.Context) {
		defer close(exit)
		for i := 0; i < 2; i++ {
			select {
			case <-ctx.Done():
				fmt.Println("cancel")
			default:
				time.Sleep(200 * time.Millisecond)
			}
		}
		fmt.Println("ok")
	})
	if err != nil {
		fmt.Println("err =", err.Error())
	}
	select {
	case <-exit:
	case <-ctx.Done():
		time.Sleep(10 * time.Millisecond)
	}

	// Output: ok
}
