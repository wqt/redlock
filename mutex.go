package redlock

import (
	"context"
	"encoding/hex"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"runtime"
	"runtime/debug"
	"sync"
	"sync/atomic"
	"time"

	"github.com/google/uuid"
)

const (
	// 默认的请求超时时间
	defaultTimeout = 50 * time.Millisecond
	// 默认的过期时间
	defaultExpires = 30 * time.Second
	// 默认的重试次数
	defaultRetries = 1
)

var errLogger Logger

type Logger interface {
	Error(err error)
}

var _ Logger = (*StdLogger)(nil)

type StdLogger struct{}

func (l *StdLogger) Error(err error) {
	if err == nil {
		return
	}
	pc, file, line, ok := runtime.Caller(1)
	if ok {
		log.Printf("%s#%d-%s: %+v", file, line, runtime.FuncForPC(pc).Name(), err)
	}
}

var (
	defaultDelay = randomDelay()
	once         sync.Once
)

// SetLogger 设置日志, 如果l==nil,则不打印日志
func SetLogger(l Logger) {
	once.Do(func() {
		errLogger = l
	})
}

func outputError(err error) {
	if errLogger == nil {
		return
	}
	if err == nil {
		return
	}
	pc, file, line, ok := runtime.Caller(1)
	if ok {
		log.Printf("%s#%d-%s: %+v", file, line, runtime.FuncForPC(pc).Name(), err)
		return
	}
	log.Printf("%+v\n%s", err, debug.Stack())
}

// debug标记
type debugFlag struct{}

var _debugFlag *debugFlag

// randomDelay 产生一个[2-10)的随机延迟
func randomDelay() time.Duration {
	random := rand.New(rand.NewSource(time.Now().UnixNano()))
	n := random.Int63n(10)
	if n < 2 {
		n = 2
	}
	return time.Duration(n) * time.Millisecond
}

// Options 请求锁时的选项
type Options struct {
	// 请求超时时间
	timeout time.Duration
	// 有效时间
	expires time.Duration
	// 重试次数,
	retries int
	// 时间偏移
	delay time.Duration
	// 连接池
	conns []Conn
}

var optsionPool = sync.Pool{
	New: func() interface{} {
		return &Options{
			timeout: defaultTimeout,
			expires: defaultExpires,
			retries: defaultRetries,
			delay:   defaultDelay,
			conns:   nil,
		}
	},
}

// Option 选项函数
type Option func(*Options)

func (o *Options) reset() {
	o.timeout = defaultTimeout
	o.expires = defaultExpires
	o.retries = defaultRetries
	o.delay = defaultDelay
	o.conns = nil
}

// applyOptions 合并选项
func applyOptions(opts ...Option) *Options {
	v := optsionPool.Get()
	o := v.(*Options)
	o.reset()
	for _, opt := range opts {
		opt(o)
	}
	return o
}

// WithTimeout 设置请求超时时间
func WithTimeout(d time.Duration) Option {
	return func(o *Options) {
		if d > 0 {
			o.timeout = d
		}
	}
}

// WithExpires 设置过期时间
func WithExpires(d time.Duration) Option {
	return func(o *Options) {
		if d > 0 {
			o.expires = d
		}
	}
}

// WithRetries 设置重试次数
func WithRetries(n int) Option {
	return func(o *Options) {
		if n >= 0 {
			o.retries = n
		}
	}
}

// WithConnections 设置客户端连接, 并不检查是否是nil, 传入的数量就是所有的redis实例数
func WithConnections(conns ...Conn) Option {
	return func(o *Options) {
		o.conns = append(o.conns, conns...)
	}
}

// Mutex 互斥锁
type Mutex struct {
	// 申请锁的名称
	name string
	// 生成的值
	value string
	// 失败的数量
	failed uint32
	// 成功的数量
	success uint32
	// 成功获取锁的时间
	last time.Time

	opts *Options
}

// newMutex 创建一个新的互斥锁,参数name是认证的唯一标识,如:
//	 userid:051c6b48-7a40-4826-850d-ea500adb7ebc
func NewMutex(name string, opts ...Option) (*Mutex, error) {
	o := applyOptions(opts...)
	if len(o.conns) == 0 {
		return nil, errors.New("must provider redis connections")
	}
	return newMutex(name, o), nil
}

func newMutex(name string, opts *Options) *Mutex {
	value := uuid.New()
	return &Mutex{
		name:  name,
		value: hex.EncodeToString(value[:]),
		opts:  opts,
	}
}

// getMinSuccess 计算锁请求的最小成功实例数量
func (m *Mutex) getMinSuccess() int {
	return len(m.opts.conns)/2 + 1
}

// tryLockOrExtend 尝试一次加锁或者延长锁的有效期
func (m *Mutex) tryLockOrExtend(ctx context.Context, extend bool) (ok bool, err error) {
	// 加锁成功的数量
	minSuccess := m.getMinSuccess()
	maxFail := len(m.opts.conns) - minSuccess
	// 接收多个错误
	errs := make(MultiError, len(m.opts.conns))
	// 设置超时时间
	ctx, cancel := context.WithTimeout(ctx, m.opts.timeout)
	defer cancel()
	// 并发执行加锁
	var g sync.WaitGroup
	for i := 0; i < len(m.opts.conns); i++ {
		i := i
		conn := m.opts.conns[i]
		// 直接跳过
		if conn == nil {
			continue
		}
		g.Add(1)
		go func() {
			defer g.Done()
			var err error
			if extend {
				err = conn.Extend(ctx, m.name, m.value, m.opts.expires)
			} else {
				err = conn.SetNX(ctx, m.name, m.value, m.opts.expires)
			}
			if err == nil {
				atomic.AddUint32(&m.success, 1)
				return
			}
			errs[i] = err
			// 失败次数足够多
			if atomic.AddUint32(&m.failed, 1) >= uint32(maxFail) {
				cancel()
			}
		}()
	}
	g.Wait()
	if _debugFlag != nil {
		m.last = time.Now()
	}
	if err = errs.Unwrap(); err != nil {
		err = fmt.Errorf("tryLockOrExtend %w", err)
	}
	if m.success >= uint32(minSuccess) {
		ok = true
	}
	return
}

// lockOrExtend 尝试加锁或者延长有效期
func (m *Mutex) lockOrExtend(ctx context.Context, extend bool) error {
	// 开始加锁的时间
	start := time.Now()
	num := m.opts.retries + 1
	errs := make(MultiError, 0, num)
	// 最大尝试m.opts.retries+1次
	for i := 0; i < num; i++ {
		// 每次重试都检查是不是上游退出或者超时
		select {
		case <-ctx.Done():
			errs = append(errs, ctx.Err())
		default:
		}
		ok, err := m.tryLockOrExtend(ctx, extend)
		if err != nil && errLogger != nil {
			outputError(err)
		}
		if ok {
			// 计算加锁使用的时间
			elapsed := time.Since(start)
			// 加锁使用的时间小于锁的过期时间减去随机的延迟, 认为是成功的
			if elapsed < m.opts.expires-m.opts.delay {
				m.last = start
				return nil
			}
		}
		errs = append(errs, err)
		// 全部解锁
		if err1 := m.Unlock(ctx); err1 != nil {
			if errLogger != nil {
				outputError(err)
			}
		}
	}
	if err := errs.Unwrap(); err != nil {
		return err
	}
	return errors.New("lock failed")
}

// Lock 执行加锁
func (m *Mutex) Lock(ctx context.Context) error {
	return m.lockOrExtend(ctx, false)
}

// lock 延长锁的有效期, 只有在加锁成功后调用才能成功
func (m *Mutex) Extend(ctx context.Context) error {
	return m.lockOrExtend(ctx, true)
}

// Unlock 执行解锁
func (m *Mutex) Unlock(ctx context.Context) error {
	// 将选项放回缓冲池
	defer func() {
		optsionPool.Put(m.opts)
	}()
	// 锁超时了就执行退出
	if d := time.Since(m.last); d >= m.opts.expires {
		return nil
	}
	// 接收多个错误
	errs := make(MultiError, len(m.opts.conns))
	// 设置超时时间
	ctx, cancel := context.WithTimeout(ctx, m.opts.timeout)
	defer cancel()
	// 并发执行解锁锁
	var g sync.WaitGroup
	for i := 0; i < len(m.opts.conns); i++ {
		i := i
		conn := m.opts.conns[i]
		if conn == nil {
			continue
		}
		g.Add(1)
		go func() {
			defer g.Done()
			if err := m.unlockOne(ctx, conn); err != nil {
				errs[i] = err
			}
		}()
	}
	g.Wait()
	if err := errors.Unwrap(errs); err == nil {
		return err
	}
	return nil
}

func (m *Mutex) unlockOne(ctx context.Context, conn Conn) error {
	var err error
	// 接收多个错误
	retries := m.opts.retries + 1
	errs := make(MultiError, 0, retries)
	// 和加锁时一样, 最多执行m.opts.retries+1次
	// 每个实例都要重试, 直到成功或者达到最大次数
L1:
	for i := 0; i < retries; i++ {
		select {
		case <-ctx.Done():
			errs = append(errs, ctx.Err())
			break L1
		default:
		}
		if err = conn.Delete(ctx, m.name, m.value); err != nil {
			errs = append(errs, err)
		}
	}
	return errs.Unwrap()
}

// LastTime 返回加锁成功的时间
func (m *Mutex) LastTime() time.Time {
	return m.last
}

// Exec 加锁并执行
// 传入的f函数如果执行时间过长, 会尝试延续锁的有效期,
// 延长有效期失败就退出,至于执行的操作是否有中断方法需要在f函数内部使用context
func (m *Mutex) Exec(ctx context.Context, f WorkerFunc) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = fmt.Errorf("lock panic: %v", e)
		}
	}()
	defer func() {
		if err1 := m.Unlock(ctx); err1 != nil {
			outputError(fmt.Errorf("unlock %w", err1))
		}
	}()
	// 需要按需延长锁的有效期
	ctx1, cancel := context.WithCancel(ctx)
	defer cancel()
	if err1 := m.Lock(ctx1); err1 != nil {
		return fmt.Errorf("lock error: %w", err1)
	}

	// 在剩余有效期一半的时间延长锁
	refreshTime := (m.opts.expires - time.Since(m.last)) / 2
	done := make(chan struct{})
	go func() {
		f(ctx1)
		// 执行结束关闭通道
		close(done)
	}()
	tick := time.NewTicker(refreshTime)
	defer tick.Stop()
	for {
		select {
		case <-done:
			return
		case <-ctx1.Done():
			return ctx.Err()
		case <-tick.C:
			// 延长有效期失败就退出,至于执行的操作是否有中断方法需要在f函数内部使用context
			if err1 := m.Extend(ctx1); err1 != nil {
				return err1
			}
			tick.Reset(refreshTime)
		}
	}
}
